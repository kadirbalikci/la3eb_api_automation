package com.La3ebAPI.step_definitions;

import com.La3ebAPI.utilities.ConfigurationReader;
import com.La3ebAPI.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Map;
import static io.restassured.RestAssured.given;

public class Alpha_Geo_Checkout_StepDefs {

    Response response;
    Response response3;
    Map<String,Object> handhsakeMap = new HashMap<>();

    @Given("Get Handshake Token")
    public void get_Handshake_Token() {
        handhsakeMap.put("Content-Type", "application/json");
        handhsakeMap.put("x-app-version","1.4.1");

        Map <String, Object> bodyMap = new HashMap<>();
        bodyMap.put("store", "la3ebuaeen");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(handhsakeMap)
                .and().body(bodyMap)
                .when().post("/handshake");
        handhsakeMap.put("x-access-token", response.body().path("token"));
        System.out.println(response.body().path("token").toString());
    }

    @Given("Empty the Cart")
    public void empty_the_Cart() {
       response =  given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .headers(handhsakeMap).
                when().post("/cart/empty-cart");
        System.out.println(response.statusCode());
    }

    @When("Put items to the Cart with {int}")
    public void put_items_to_the_Cart_with(Integer qty) {
        Map <String, Object> cartMap = new HashMap<>();
        Map <String, Object> itemsMap = new HashMap<>();
        itemsMap.put("sku",ConfigurationReader.get("alpha_sku"));
        itemsMap.put("qty", qty);

        Map [] items = {itemsMap};
        cartMap.put("items", items);
        cartMap.put("cart_source", "wallet");

        Response response2 = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .headers(handhsakeMap).
                        and().body(cartMap).
                        when().put("/cart");
        String body = response2.body().prettyPrint();
        System.out.println(response2.prettyPrint());
    }

    @When("Post Billing Address")
    public void post_Billing_Address() {
        Map<String, Object> billingMap = new HashMap<>();

        billingMap.put("firstName", "KadirB");
        billingMap.put("lastName","AUTOMATION");
        billingMap.put("telephone","+96651234578");
        billingMap.put("countryId","SA");
        billingMap.put("email","kadirbalikci@gmail.com");
        billingMap.put("save", false);

        Response response1 =  given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .headers(handhsakeMap).
                        and().body(billingMap).
                        when().post("/cart/billing-address");
        int statusCode = response1.statusCode();
        System.out.println(response1.prettyPrint());
    }

    @When("Put Payment Method for CC")
    public void put_Payment_Method_for_CC() {

        Map<String, Object> paymentMethodMap = new HashMap<>();

        paymentMethodMap.put("method", "checkoutcom_card_payment");
        paymentMethodMap.put("email","kadirbalikci@gmail.com");

        Response response5 =  given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .headers(handhsakeMap).
                        and().body(paymentMethodMap).
                        when().put("/cart/payment-method");
    }
    
    @Then("Post Checkout Tokens")
    public void post_Checkout_Tokens() {
        Map <String, Object> CKO_Headers = new HashMap<>();
        CKO_Headers.put("Content-Type","application/json");
        CKO_Headers.put("Authorization","pk_test_5698ab73-00ff-420a-be2e-ba100c2dd5ab");

        Map<String, Object> CKO_Body = new HashMap<>();
        CKO_Body.put("number","4485040371536584");
        CKO_Body.put("expiry_month",12);
        CKO_Body.put("expiry_year",2021);
        CKO_Body.put("cvv","100");
        CKO_Body.put("type","card");

        response3 = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(CKO_Headers)
                .and().body(CKO_Body)
                .when().post("https://api.sandbox.checkout.com/tokens");
        handhsakeMap.put("x-access-token", response.body().path("token"));
        System.out.println("token status code"+response3.statusCode());
        System.out.println("response3.prettyPrint() = " + response3.prettyPrint());
    }

    @Then("Put Place Order")
    public void put_Place_Order() throws InterruptedException {
//        Map<String, Map<String, Object>> placeOrderBody = new HashMap<>();
//        Map<String, Object> paymentMethod = new HashMap<>();
//        String CardToken = response3.body().path("token");
//        System.out.println("token = " + CardToken);
//        paymentMethod.put("method","checkoutcom_card_payment");
//        paymentMethod.put("token", CardToken);
//        paymentMethod.put("cardBin","448504");
//        paymentMethod.put("success_url","https://alpha-web.la3eb.com/en-sa/checkout/success?utm_nooverride=1");
//        paymentMethod.put("failure_url","https://alpha-web.la3eb.com/en-sa/checkout/failure?utm_nooverride=1");
//        placeOrderBody.put("paymentMethod", paymentMethod);
//
//        Response response4 =  given().contentType(ContentType.JSON)
//                .and().accept(ContentType.JSON)
//                .headers(handhsakeMap).
//                        and().body(placeOrderBody).
//                        when().put("/cart/place-order");
//        Thread.sleep(5000);
//        System.out.println(response4.prettyPrint());
//        System.out.println(response4.statusCode());
//        Driver.get().get(response4.body().path("threeDS.redirectUrl"));
//        Thread.sleep(5000);

        Map<String, Map<String, Object>> placeOrderBody = new HashMap<>();
        Map<String, Object> paymentMethod = new HashMap<>();
        String CardToken = response3.body().path("token");

        paymentMethod.put("method","checkoutcom_card_payment");
        paymentMethod.put("token", CardToken);
        paymentMethod.put("cardBin","448504");
        paymentMethod.put("save",false);
        paymentMethod.put("success_url","https://alpha-web.la3eb.com/en-sa/checkout/success?utm_nooverride=1");
        paymentMethod.put("failure_url","https://alpha-web.la3eb.com/en-sa/checkout/failure?utm_nooverride=1");

        placeOrderBody.put("paymentMethod", paymentMethod);

        Response response4 =  given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .headers(handhsakeMap).
                        and().body(placeOrderBody).
                        when().put("/cart/place-order");
        Thread.sleep(5000);
        System.out.println(response4.statusCode());
        Driver.get().get(response4.body().path("threeDS.redirectUrl"));
        Thread.sleep(5000);
        // Driver.get().get(response4.body().path("threeDS.redirectUrl"));
        System.out.println(response4.prettyPrint());

    }
}
