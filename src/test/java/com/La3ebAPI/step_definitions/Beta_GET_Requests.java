package com.La3ebAPI.step_definitions;

import com.La3ebAPI.utilities.ConfigurationReader;
import com.google.gson.Gson;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class Beta_GET_Requests {

    Response response;
    Response response3;
    Map<String,Object> headersMap = new HashMap<>();
    Map<String,Object> queryParamMap = new HashMap<>();

    @Given("Handshake token is received")
    public void handshakeTokenIsReceived() {
        headersMap.put("Content-Type", "application/json");
        headersMap.put("x-app-version","1.4.1");

        Map <String, Object> bodyMap = new HashMap<>();
        bodyMap.put("store", "la3eben");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .and().body(bodyMap)
                .when().post("/handshake");
        headersMap.put("x-access-token", response.body().path("token"));
        System.out.println(response.body().path("token").toString());
    }
    @Given("Get Categories Endpoint")
    public void getCategoriesEndpoint() {
       // headersMap.put("Accept","application/json");
        headersMap.put("X-Device-Os-Type","swagger");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/categories");
       // System.out.println(response.body().prettyPrint());
        Assert.assertEquals(200, response.statusCode());
        int id = response.path("id");
        Assert.assertEquals(423, id);
        Assert.assertEquals("La3eb", response.path("name"));
    }


    @And("Get Category id")
    public void getCategoryId() {
        headersMap.put("X-Device-Os-Type","swagger");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/category/"+ ConfigurationReader.get("category_id"));
        //System.out.println(response.body().prettyPrint());

        Assert.assertEquals(200, response.statusCode());
        Assert.assertEquals("basic_catalog", response.path("type[0]"));
    }

    @Given("Get Customer with specific id")
    public void getCustomerWithSpecificId() {
        headersMap.put("X-Device-Os-Type","swagger");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/customer/"+ ConfigurationReader.get("customer_id"));
       // System.out.println(response.body().prettyPrint());
        Assert.assertEquals(200, response.statusCode());
        Assert.assertEquals(ConfigurationReader.get("customer_id"), response.path("id").toString());
    }

    @Given("Get products of a specific id")
    public void getProductsOfASpecificId() {
        headersMap.put("X-Device-Os-Type","swagger");
        queryParamMap.put("limit",10);
//        queryParamMap.put("offset",1);
//        queryParamMap.put("sortBy","price");
//        queryParamMap.put("filterBy","color");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .and().queryParams(queryParamMap)
                .when().get("/category/"+ ConfigurationReader.get("category_id")+"/products");
    //     System.out.println(response.body().prettyPrint());
        Assert.assertEquals(200, response.statusCode());
        Assert.assertTrue(response.path("items[0].sku").toString().contains("LC-GO"));
    }

    @Given("Get reviews of a specific sku")
    public void getReviewsOfASpecificSku() {
        headersMap.put("X-Device-Os-Type","swagger");
        queryParamMap.put("limit",10);

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .and().queryParams(queryParamMap)
                .when().get("/product/"+ ConfigurationReader.get("sku")+"/reviews");
             System.out.println(response.body().prettyPrint());
        Assert.assertEquals(200, response.statusCode());
        Assert.assertFalse(response.path("reviews").toString().isEmpty());
    }

    @And("{string} is added to cart one by one")
    public void isAddedToCartOneByOne(String itemSKu) {

        headersMap.put("X-Device-Os-Type","swagger");

        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .and().request().body("{\n" +
                        "  \"items\": [\n" +
                        "    {\n" +
                        "      \"sku\": \""+itemSKu+"\",\n" +
                        "      \"qty\": \"1\"\n" +
                        "    }\n" +
                        "  ],\n" +
                        "  \"cart_source\": \"wallet\"\n" +
                        "}")
                .when().put("/cart");
        // System.out.println(response.body().prettyPrint());
        Assert.assertEquals(201, response.statusCode());
        if (response.statusCode()==400){
            System.out.println("Error message: "+ response.path("message").toString());
        }
    }

    @Given("Get product details of a specific sku")
    public void getProductDetailsOfASpecificSku() {
        headersMap.put("X-Device-Os-Type","swagger");
        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/products/"+ ConfigurationReader.get("sku"));
       // System.out.println(response.body().prettyPrint());
        Assert.assertEquals(200, response.statusCode());
        System.out.println("id = " + response.path("id"));
        Assert.assertEquals(ConfigurationReader.get("id"), response.path("id"));
        Assert.assertEquals(ConfigurationReader.get("sku"), response.path("sku"));
    }

    @Given("Get available payment methods")
    public void getAvailablePaymentMethods() {
        headersMap.put("X-Device-Os-Type","swagger");
        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/cart/available-payment-methods");
         System.out.println(response.body().prettyPrint());
        String EngCreditCard = "Credit / Debit Card / mada bank card";
        String ArCreditCard = "البطاقات الائتمانية - مدى";
        Assert.assertEquals(200, response.statusCode());
        Assert.assertEquals(EngCreditCard, response.path("paymentMethods[0].english_name"));
        Assert.assertEquals(ArCreditCard, response.path("paymentMethods[0].arabic_name"));
        Assert.assertEquals(EngCreditCard, response.path("paymentMethods[4].english_name"));
        Assert.assertEquals("checkoutcom_card_payment", response.path("paymentMethods[4].method"));
        Assert.assertEquals("Installment", response.path("paymentMethods[1].english_name"));
        Assert.assertEquals("Cash On Delivery", response.path("paymentMethods[2].english_name"));
        Assert.assertEquals("الدفع عند التسليم", response.path("paymentMethods[2].arabic_name"));
        int feesCOD = response.path("paymentMethods[2].fees");
        Assert.assertEquals(25, feesCOD);
        Assert.assertEquals("PayPal Express Checkout", response.path("paymentMethods[5].english_name"));
    }

    @Given("Get available shipping methods")
    public void getAvailableShippingMethods() {
        headersMap.put("X-Device-Os-Type","swagger");
        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/cart/estimate-shipping-methods");
        System.out.println(response.body().prettyPrint());
        Assert.assertEquals("flatrate", response.path("shipping-methods.home-delivery.shipping_carrier_code"));
        Assert.assertEquals("25", response.path("shipping-methods.home-delivery.shipping_cost"));
        Assert.assertEquals("storepickup", response.path("shipping-methods.store-pickup.shipping_carrier_code"));
        int storepickupCost = response.path("shipping-methods.store-pickup.shipping_cost");
        Assert.assertEquals(0, storepickupCost);
    }

    @Given("Get details of a social channel")
    public void getDetailsOfASocialChannel() {
        headersMap.put("X-Device-Os-Type","swagger");
        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headersMap)
                .when().get("/social/channel/" + ConfigurationReader.get("channel_code"));
        System.out.println(response.body().prettyPrint());

        Assert.assertEquals(ConfigurationReader.get("channel_code"), response.path("code"));
        Assert.assertEquals(ConfigurationReader.get("channel_name"), response.path("name"));
        Assert.assertEquals("Game Channel", response.path("type"));
    }
}
