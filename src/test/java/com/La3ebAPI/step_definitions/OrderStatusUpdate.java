package com.La3ebAPI.step_definitions;
import com.La3ebAPI.utilities.ConfigurationReader;
import com.La3ebAPI.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Map;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.head;

public class OrderStatusUpdate {
    public OrderStatusUpdate() { }
    Response response;
    Map<String,Object> headers = new HashMap<>();

    @Given("Sending POST request for orderActions webhook endpoint with specific {string}")
    public void sending_POST_request_for_orderActions_webhook_endpoint_with_specific(String orderID) {

        headers.put("authorization", "Bearer tcp0hwrbu6xlfyumn009njunopv9qf77");
        headers.put("cache-control", "no-cache");
        headers.put("content-type", "application/json");
        headers.put("postman-token", "e808f653-e758-5236-9c9f-7f99335296c5");
        headers.put("Cookie", "PHPSESSID=t9k16tv0cgshd0lt0i3kbd0elb");
        headers.put("Accept", "*/*");

        Map <String, Map<String, Object>> bodyMap = new HashMap<>();
        Map<String, Object> orderBody = new HashMap<>();
        orderBody.put("sap_order_id", orderID);
        bodyMap.put("order", orderBody);

        System.out.println("orderBody = " + orderBody);
        System.out.println("bodymap = " + bodyMap);
        System.out.println(headers);
        response = given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .and().headers(headers)
                .and().body(("{\"order\":{\"sap_order_id\":\"" +orderID+ "\"}}"))
                .when().post("/sap/orderActions");

        System.out.println(response.statusCode());
        System.out.println("response = " + response.prettyPrint() );

    }
//  https://beta-api.la3eb.com/api/V1

}
