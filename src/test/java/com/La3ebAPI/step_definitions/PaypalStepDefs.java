package com.La3ebAPI.step_definitions;
import com.La3ebAPI.utilities.ConfigurationReader;
import com.La3ebAPI.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Map;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

public class PaypalStepDefs {

    ApiStepDefs apiStepDefs = new ApiStepDefs();

    @Then("Put Payment Method")
    public void put_Payment_Method() {

        String paymentMethodBody = "{\n" +
                " \"email\" : \"kadirbalikci@hotmail.com\",\n" +
                " \"paymentMethod\" : {\n" +
                "   \"method\" : \"paypal_express\"\n" +
                " }\n" +
                "}";

        Response response5 =  given().contentType(ContentType.JSON)
                .and().accept(ContentType.JSON)
                .headers(apiStepDefs.handhsakeMap).
                        and().body(paymentMethodBody).
                        when().put("/cart/payment-method");


    }

//    @Then("Generate Payment URL")
//    public void generate_Payment_URL() {
//        Map<String, Object> GenerateUrlBody = new HashMap<>();
//
//        GenerateUrlBody.put("amount", 198);
//        GenerateUrlBody.put("cancel_url", "com.leanscale.la3eb.PayPalReturn:\\/\\/cancel");
//        GenerateUrlBody.put("success_url", "com.leanscale.la3eb.PayPalReturn:\\/\\/return");
//
//        Response response6 =  given().contentType(ContentType.JSON)
//                .and().accept(ContentType.JSON)
//                .headers(apiStepDefs.handhsakeMap).
//                        and().body(GenerateUrlBody).
//                        when().put("/cart/generate-payment-url");
//        System.out.println(response6.prettyPrint());
//        System.out.println(response6.statusCode());
//    }

    @Then("Get Token from Paypal")
    public void get_Token_from_Paypal() {

    }
}
