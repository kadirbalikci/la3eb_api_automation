Feature: Beta Get Requests Should Work As Expected

  Background: Handshake is performed
    Given Handshake token is received

  @wip
  Scenario: Beta Get Requests Unit Tests
    Given Get Categories Endpoint

    Scenario: Get Category with specific id
    Given Get Category id

  Scenario: Get Customer with specific id
    Given Get Customer with specific id

  Scenario: Get category/id/products
    Given Get products of a specific id

  Scenario: Get /product/sku/reviews
    Given Get reviews of a specific sku

  Scenario: Get /products/search
    Given Get reviews of a specific sku

  Scenario: Get /products/[sku]
    Given Get product details of a specific sku

  Scenario: Get /cart/available-payment-methods
    Given Get available payment methods

  Scenario: Get /cart/estimate-shipping-methods
    Given Get available shipping methods

  Scenario: Get /social/channel/[code]")
    Given Get details of a social channel

