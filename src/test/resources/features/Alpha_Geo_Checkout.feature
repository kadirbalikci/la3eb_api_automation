Feature: Alpha Geo Expansion Checkout Flow

  @wip
  Scenario Outline: La3ebUaeen Store digital checkout flow
    Given Get Handshake Token
    And Empty the Cart
    When Put items to the Cart with <qty>
    And Post Billing Address
    And Put Payment Method for CC
    Then Post Checkout Tokens
    And Put Place Order

    Examples:
      | qty |
      |  1  |