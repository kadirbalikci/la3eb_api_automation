Feature: Read/Get Function Positive and Negative Test Cases

  @beta
  Scenario Outline: verify getting right item with valid values
    Given User adds a product to cart
    When User places an order
    And Shipping Info entered with the <qty>
    Then status should be success
    And Get Token from CKO
    And Complete Order
    Examples:
      | qty |
    |    1 |
    |    1 |
    |    1 |
#      |    4 |
#      |    5 |
#      |    1 |
#      |    1 |
#      |    1 |
#      |    1 |
#      |    1 |
